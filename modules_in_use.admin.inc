<?php

/*
 * Display a list of all modules enabled.
 */
function modules_enabled_list() {
  $modules_in_use = module_list();
  $title = t('Modules Currently Enabled on this Site');
  $type = 'ol';
  $attributes = array(
    'id' => 'enabled-module-list',
    'class' => 'items-list',
  );
  $items = $modules_in_use;

  return theme_item_list(array('items' => $items, 'title' => $title, 'type' => $type, 'attributes' => $attributes));
}

/*
 * Attempt to display a list of all modules enabled, but not in use 
 */
function modules_not_in_use(){
  $title = t('Modules Enabled, but not in Use');
  $type = 'ol';
  $attributes = array(
    'id' => 'not-in-use-module-list',
    'class' => 'items-list',
  );
  $items = array();
  
  // If we can't find at least one blog post, the blog module is not being used.
  $blog_post_exists = _check_any_blog_post_exists();
  if (!$blog_post_exists) {
    $items[] = 'blog';
  }
  $aggregator_feed_exists = _check_any_aggregator_feed_exists();
  if (!$aggregator_feed_exists) {
    $items[] = 'aggregator';
  }
  
  // No modules could be found that were not being utilized.
  if (empty($items)) {
    return ('<h3>'.$title.'</h3>' . t('<p>All modules tested appear to be in use.</p>'));
  }
  return theme_item_list(array('items' => $items, 'title' => $title, 'type' => $type, 'attributes' => $attributes));
}

/**
 * Builds a form of modules considered not in use.
 *
 * @ingroup forms
 * @see modules_not_in_use_form_validate()
 * @see modules_not_in_use_form_submit()
 * @param $form_state['values']
 *   Submitted form values.
 * @return
 *   A form array representing modules considered not in use.
 */
function modules_not_in_use_form($form, $form_state = NULL) {
  
  // Determine which modules are not in use
  $all_modules = system_rebuild_module_data();
  $unused = array();
  // Is the blog module currently enabled
  if(module_exists('blog')) {
    // If we can't find at least one blog post, the blog module is not being used.
    $blog_post_exists = _check_any_blog_post_exists();
    if (!$blog_post_exists) {
      $unused['blog'] = $all_modules['blog'];
    }
  }
  if(module_exists('aggregator')) {  
    $aggregator_feed_exists = _check_any_aggregator_feed_exists();
    if (!$aggregator_feed_exists) {
      $unused['aggregator'] = $all_modules['aggregator'];
    }
  }

  // Only build the rest of the form if there are any modules available to
  // disable.
  if (!empty($unused)) {
    $profile = drupal_get_profile();
    $form['unused'] = array('#tree' => TRUE);
    foreach ($unused as $module) {
      $module_name = $module->info['name'] ? $module->info['name'] : $module->name;
      $form['modules'][$module->name]['#module_name'] = $module_name;
      $form['modules'][$module->name]['name']['#markup'] = $module_name;
      $form['modules'][$module->name]['description']['#markup'] = t($module->info['description']);
      $form['unused'][$module->name] = array(
        '#type' => 'checkbox',
        '#title' => t('Disable @module module', array('@module' => $module_name)),
        '#title_display' => 'invisible',
      );
      // All modules which depend on this one must be disabled first, before
      // we can allow this module to be disabled. (The installation profile
      // is excluded from this list.)
      foreach (array_keys($module->required_by) as $dependent) {
        if ($dependent != $profile && drupal_get_installed_schema_version($dependent) != SCHEMA_UNINSTALLED) {
          $dependent_name = isset($all_modules[$dependent]->info['name']) ? $all_modules[$dependent]->info['name'] : $dependent;
          $form['modules'][$module->name]['#required_by'][] = $dependent_name;
          $form['unused'][$module->name]['#disabled'] = TRUE;
        }
      }
    }
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Disable'),
    );
    //$form['#action'] = url('admin/modules/disable/confirm');
  }
  else {
    $form['modules'] = array();
  }

  return $form;
}

/**
 * Validates the submitted module disable form.
 */
function modules_not_in_use_form_validate($form, &$form_state) {
  // dpm($form);
  // Form submitted, but no modules selected.
  if (!count(array_filter($form_state['values']['unused']))) {
    drupal_set_message(t('No modules selected.'), 'error');
    drupal_goto('admin/modules/not-in-use');
  }
}

/**
 * Processes the submitted module disable form.
 */
function modules_not_in_use_form_submit($form, &$form_state) {
  // dpm($form);
  if (!empty($form['unused'])) {
    // Call the disable routine for each selected module.
    $modules = array_keys($form_state['values']['unused']);
    module_disable($modules, $disable_dependents = FALSE);
    drupal_set_message(t('The selected modules have been disabled.'));

    $form_state['redirect'] = 'admin/modules/not-in-use';
  }
  else {
    $form_state['storage'] = $form_state['values'];
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Returns HTML for a table of modules considered not in use.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 *
 * @ingroup themeable
 */
function theme_modules_not_in_use_form($variables) {
  $form = $variables['form'];

  // No theming for the confirm form.
  if (isset($form['confirm'])) {
    return drupal_render($form);
  }

  // Table headers.
  $header = array(t('Disable'),
    t('Name'),
    t('Description'),
  );

  // Display table.
  $rows = array();
  foreach (element_children($form['modules']) as $module) {
    if (!empty($form['modules'][$module]['#required_by'])) {
      $disabled_message = format_plural(count($form['modules'][$module]['#required_by']),
        'To disable @module, the following module must be disabled first: @required_modules',
        'To disable @module, the following modules must be disabled first: @required_modules',
        array('@module' => $form['modules'][$module]['#module_name'], '@required_modules' => implode(', ', $form['modules'][$module]['#required_by'])));
      $disabled_message = '<div class="admin-requirements">' . $disabled_message . '</div>';
    }
    else {
      $disabled_message = '';
    }
    $rows[] = array(
      array('data' => drupal_render($form['unused'][$module]), 'align' => 'center'),
      '<strong><label for="' . $form['unused'][$module]['#id'] . '">' . drupal_render($form['modules'][$module]['name']) . '</label></strong>',
      array('data' => drupal_render($form['modules'][$module]['description']) . $disabled_message, 'class' => array('description')),
    );
  }

  $output  = theme('table', array('header' => $header, 'rows' => $rows, 'empty' => t('All enabled modules are considered "in use", and therefore there\'s nothing to disable.')));
  $output .= drupal_render_children($form);

  return $output;
}

function _check_any_blog_post_exists() {
  // Question: what to do if there are blog pages, but all blogs pages are unpublished?
  // For now, unpublished = Not in use.
  return (bool) db_select('node', 'n')->fields('n', array('nid'))->condition('type', 'blog')->condition('status', 1)->range(0, 1)->execute()->fetchField();
}
function _check_any_aggregator_feed_exists() {
  return (bool) db_select('aggregator_feed', 'a')->fields('a', array('fid'))->range(0, 1)->execute()->fetchField();
}
