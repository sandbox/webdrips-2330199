Modules In Use
==============

Having modules enabled, but not in use, can waste precious resources on your 
server, add security loopholes, and cause confusion for content editors and 
developers.

This module attempts to correct that by assessing whether various enabled 
modules are actually in use. For example, if the core blog module is enabled, 
does at least one blog post exist? Currently this module simply lists modules 
enabled, and modules enabled, but not in use. Future revisions of the module 
will allow modules not in use to be disabled in bulk and include Drush 
integration.

To view a list of modules, from the module admin page, click the "Modules
enabled" tab (or go to /admin/modules/enabled). To view modules not in use, 
click the "Modules not in use" tab (or go to /admin/modules/not-in-use).
